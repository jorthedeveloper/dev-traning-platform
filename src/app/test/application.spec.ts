// System 
import { TestBed, async } from '@angular/core/testing';

// Modules
import { RouterTestingModule } from '@angular/router/testing';
import { EntityModule } from '../core/entity/entity.module';
import { InteractorModule } from '../core/business-rules/interactor/interactor.module';

// Class
import { LogoutUser } from '../core/business-rules/interactor/logout-user';

// Services
import { DeleteSessionEntityService } from '../core/entity/delete-session-entity.service';
import { LogoutUserDeliveryService } from '../core/business-rules/interactor/services/logout-user.service';

// Components
import { AppComponent } from '../app.component';





describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'dev-traning-platform'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('dev-traning-platform');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.content span').textContent).toContain('dev-traning-platform app is running!');
  });
});

describe('Logout User Process', () => {
    let deliveryService: LogoutUserDeliveryService;
    let logoutUserInstance: LogoutUser;
    beforeEach(()=> {
      TestBed.configureTestingModule({
        imports: [InteractorModule, EntityModule],
        providers: [LogoutUserDeliveryService, DeleteSessionEntityService],
      });
      
    });

    afterEach(() => {
      deliveryService = null;
      logoutUserInstance = null;
    })

    it('should create a LogoutUserDeliveryService instance', () => {
      deliveryService = new LogoutUserDeliveryService();
      expect(deliveryService).toBeInstanceOf(LogoutUserDeliveryService);
    });

    it('should create a LogoutUserClass instance', () => {
      logoutUserInstance = new LogoutUser();
      expect(logoutUserInstance).toBeInstanceOf(LogoutUser);
    });

    it('should create a DeleteSessionEnitiyService instance', () => {
      let session: string
      let deleteSessionEnitiyServiceInstance = new DeleteSessionEntityService(session);
      expect(deleteSessionEnitiyServiceInstance).toBeInstanceOf(DeleteSessionEntityService);
    })

    it('should recieve the sessionKey value by setting it',
    () => {
      let logoutUserInstance = new LogoutUser();
      logoutUserInstance.sessionKey = 'userSession';
      expect(logoutUserInstance.sessionKey).toBe('userSession');
    });

    it('should logout the user by deleting the user session', () => {
      let user: {id: 'TestId'}
      deliveryService = new LogoutUserDeliveryService();
      localStorage.setItem("userSession", JSON.stringify(user));
      deliveryService.toLogoutUserDeliveryService("userSession")
      expect(localStorage.getItem("userSession")).toBeNull();
    })

});


