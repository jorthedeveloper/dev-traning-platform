import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeliveryMechanismModule } from './delivery-mechanism/delivery-mechanism.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DeliveryMechanismModule
  ]
})
export class DetailsModule { }
