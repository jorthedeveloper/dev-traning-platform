import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoundaryObjectsModule } from '../../core/business-rules/boundary-objects/boundary-objects.module';
import { ControllerModule } from './controller/controller.module';
import { PresenterModule } from './presenter/presenter.module';
import { ViewsModule } from './views/views.module';
import { GatewayImplementationsModule } from './gateway-implementations/gateway-implementations.module';
import { APIModule } from './api/api.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    BoundaryObjectsModule,
    ControllerModule,
    ViewsModule,
    PresenterModule,
    GatewayImplementationsModule,
    APIModule
  ]
})
export class DeliveryMechanismModule { }
