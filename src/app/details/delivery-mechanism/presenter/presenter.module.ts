import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewModelModule } from '../view-model/view-model.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ViewModelModule
  ]
})
export class PresenterModule { }
