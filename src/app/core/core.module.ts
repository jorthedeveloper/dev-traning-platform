import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EntityModule } from './entity/entity.module';
import { InteractorModule } from './business-rules/interactor/interactor.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    EntityModule,
    InteractorModule
  ]
})
export class CoreModule { }
