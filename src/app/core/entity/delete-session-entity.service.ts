import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'any'
})
export class DeleteSessionEntityService {

  constructor(sessionKey: string) {
    localStorage.removeItem(sessionKey);
   }
}
