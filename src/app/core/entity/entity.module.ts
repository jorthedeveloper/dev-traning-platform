import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeleteSessionEntityService } from './delete-session-entity.service';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [DeleteSessionEntityService]
})
export class EntityModule { }
