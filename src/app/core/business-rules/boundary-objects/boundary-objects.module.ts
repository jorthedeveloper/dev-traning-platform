import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputBoundaryModule } from './input-boundary/input-boundary.module';
import { OutputBoundaryModule } from './output-boundary/output-boundary.module';
import { GatewaysModule } from './gateways/gateways.module';
import { DataModelsModule } from './data-models/data-models.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    InputBoundaryModule,
    OutputBoundaryModule,
    GatewaysModule,
    DataModelsModule
  ]
})
export class BoundaryObjectsModule { }
