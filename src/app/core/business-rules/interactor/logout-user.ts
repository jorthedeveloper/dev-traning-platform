import { DeleteSessionEntityService } from '../../entity/delete-session-entity.service';

export class LogoutUser {
    private _sessionKey:string;

    constructor(){}

    get sessionKey(): string{       
        return this._sessionKey;
    }

    set sessionKey(sessionKey: string){
        this._sessionKey = sessionKey;
    }

    toLogoutUser(){    
        return new DeleteSessionEntityService(this._sessionKey);
    }

}
