import { Injectable } from '@angular/core';
import { LogoutUser } from '../logout-user';

@Injectable({
  providedIn: 'any'
})
export class LogoutUserDeliveryService {
    constructor() {
   }

  toLogoutUserDeliveryService(sessionToken: string){
    let logout = new LogoutUser();
    logout.sessionKey = sessionToken;
    logout.toLogoutUser();
  }
}
