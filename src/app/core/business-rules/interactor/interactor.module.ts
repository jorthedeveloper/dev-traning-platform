import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogoutUserDeliveryService } from './services/logout-user.service';
import { LogoutUser } from '../interactor/logout-user';


@NgModule({
providers: [LogoutUserDeliveryService, {provide: LogoutUser, useClass: LogoutUser}],
  declarations: [  ],
  imports: [
    CommonModule
  ]
})
export class InteractorModule { }
