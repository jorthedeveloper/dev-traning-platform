# Dev Training Platform (ongoing)

#### What it is
Dev Training Platform is a training platfrom prototype that allows two users "Instructor" and "Student" to use the platfrom as a tool to create a learning atmostphere in a virtual space.

#### How it works
Functionality of the application is 'user type' dependent. Users are able create an account on the platfrom as well as login/logout from it, and depend on the user type access their specfic functionalities. Users with the user type of "Instructor" have a classroom created for them upon account creation. They have the ability to create, update and delete lessons and view the results of those lessons. Lessons take two forms, 'custom' and 'trivia'. Custom lesson allows the user to upload a video and create a custom quiz for that video, trivia lesson allows the user to generate a quiz from a quiz api based on a number of selections. 
Users with the user type of "Student" will have the ability to 'enter' an "Instructor's" classroom and view lessons as well as do them (in the form of doing a quiz) and they will have the ability to view all thier lesson results.

## Purpose of this project
The purpose of this project is to demonstrate our practical approach as software engineers in buliding software applications.

## Project Specifications

### Architecture 
The architecture follows Rober C. Martin [clean architecture methodology](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html). Where the business rules of the application is broken down into application specfic business rules and non-application specfic business rules. Both are translated into usecases and entities respectively. The usecase uses boundry objects (input ports, output ports and gateways) to send and receive data from outside the system. The architecture is centered arround the business rules of the appication which allows for third party resources (frameworks, databases etc) to act as a plugin to the system. Below is a visual representation of the [clean architecture methodology](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html) as well as a visual representation of how we will be using it in our system. 


#### Overview

![Overview](src/assets/readme/usecase-driven architecture.jpg)


#### Detailed view

![Detailed view](src/assets/readme/detailed architecture.png)


#### Prototype Implementation

![Prototype Implementation](src/assets/readme/prototype-architecture.jpg)

### Language
- Client Side -  [Angular](https://angular.io)
- Server Side -  [ASP.Net](https://dotnet.microsoft.com/apps/aspnet)
- Data Storage - [mySQL](https://www.mysql.com)

### Development Method
We have taken a test-driven development and best practices approach to buliding the platform.

## Limitations of the Prototype
- Email addresses are immutable as they will be used as unique identifiers.
- User type cannot be changed once the account is created, to change the user type, the account has to be deleted and recreated with the desired user type. 
- User does not have the ability to delete or create classroom and both functionality depends of the exsistence of the "Instructors" account.
- Lesson options that are offered are rigid in their nature, eg. custom lesson offers video and quiz as one (you can't just upload a video or just create a custom quiz).
- Store user login session in local storage.



